LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_FWK_SUPPORTS_FULL_VALUEADDS),true)
include $(CLEAR_VARS)

LOCAL_MODULE := libmmparser_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/inc

include $(BUILD_HEADER_LIBRARY)
endif

include $(CLEAR_VARS)

LOCAL_MODULE := libmmparser_proprietary_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/inc
LOCAL_PROPRIETARY_MODULE := true

include $(BUILD_HEADER_LIBRARY)
