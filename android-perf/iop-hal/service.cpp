/******************************************************************************
  @file    service.cpp
  @brief   Android IOP HAL service

  DESCRIPTION

  ---------------------------------------------------------------------------

  Copyright (c) 2017 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ---------------------------------------------------------------------------
******************************************************************************/

#define LOG_TAG "vendor.qti.hardware.iop@2.0-service"

#include <vendor/qti/hardware/iop/2.0/IIop.h>
#include <hidl/LegacySupport.h>

#include <cutils/properties.h>
#include <utils/Log.h>

#include "MpctlUtils.h"
#include <client.h>

using vendor::qti::hardware::iop::V2_0::IIop;
using android::hardware::defaultPassthroughServiceImplementation;

int main() {
    char property[PROPERTY_VALUE_MAX];
    int enableIop = 0;

    strlcpy(property, perf_get_prop("vendor.iop.enable_iop" , "0").value, PROPERTY_VALUE_MAX);
    enableIop = strtod(property, NULL);

    if(enableIop == 1)
        return defaultPassthroughServiceImplementation<IIop>();
    else
       return -1;
}
