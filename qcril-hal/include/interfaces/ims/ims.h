/******************************************************************************
#  Copyright (c) 2018 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#pragma once

#include "interfaces/common.h"
#include <string>
#include <vector>

namespace qcril {
namespace interfaces {

enum class RegState {
  UNKNOWN,
  NOT_REGISTERED, /* Not registered */
  REGISTERING,    /* Registering */
  REGISTERED      /* Registered */
};
inline std::string toString(const RegState &o) {
  if (o == RegState::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == RegState::NOT_REGISTERED) {
    return "NOT_REGISTERED";
  }
  if (o == RegState::REGISTERING) {
    return "REGISTERING";
  }
  if (o == RegState::REGISTERED) {
    return "REGISTERED";
  }
  return "<invalid>";
}
enum class VoiceInfo {
  SILENT,
  SPEECH,
  UNKNOWN
};

enum class RadioState {
  STATE_OFF,
  STATE_UNAVAILABLE,
  STATE_ON,
  STATE_UNKNOWN
};

// enum class ServiceType { INVALID, SMS, VOIP, VT };

enum class CallType {
  UNKNOWN,
  VOICE,    /* Voice */
  VT_TX,    /* Transmit only Video */
  VT_RX,    /* Receive only Vide */
  VT,       /* Video */
  VT_NODIR, /* VT no direction */
  SMS,      /* SMS */
  UT        /* UT  - Supplementary services */
};
inline std::string toString(const CallType &o) {
  if (o == CallType::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == CallType::VOICE) {
    return "VOICE";
  }
  if (o == CallType::VT_TX) {
    return "VT_TX";
  }
  if (o == CallType::VT_RX) {
    return "VT_RX";
  }
  if (o == CallType::VT) {
    return "VT";
  }
  if (o == CallType::VT_NODIR) {
    return "VT_NODIR";
  }
  if (o == CallType::SMS) {
    return "SMS";
  }
  if (o == CallType::UT) {
    return "UT";
  }
  return "<invalid>";
}

enum class StatusType {
  UNKNOWN,
  DISABLED,          /* Disabled */
  PARTIALLY_ENABLED, /* Partially enabled */
  ENABLED,           /* Enabled */
  NOT_SUPPORTED      /* Not supported */
};
inline std::string toString(const StatusType &o) {
  if (o == StatusType::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == StatusType::DISABLED) {
    return "DISABLED";
  }
  if (o == StatusType::PARTIALLY_ENABLED) {
    return "PARTIALLY_ENABLED";
  }
  if (o == StatusType::ENABLED) {
    return "ENABLED";
  }
  if (o == StatusType::NOT_SUPPORTED) {
    return "NOT_SUPPORTED";
  }
  return "<invalid>";
}

enum class RttMode {
  UNKNOWN,
  DISABLED, /* non-RTT call */
  FULL      /* RTT call */
};
inline std::string toString(const RttMode &o) {
  if (o == RttMode::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == RttMode::DISABLED) {
    return "DISABLED";
  }
  if (o == RttMode::FULL) {
    return "FULL";
  }
  return "<invalid>";
}

enum class ServiceClassStatus {
  UNKNOWN,
  DISABLED, /* Disabled */
  ENABLED,  /* Enabled */
};

inline std::string toString(const ServiceClassStatus &o) {
  if (o == ServiceClassStatus::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == ServiceClassStatus::DISABLED) {
    return "DISABLED";
  }
  if (o == ServiceClassStatus::ENABLED) {
    return "ENABLED";
  }
  return "<invalid>";
}

enum class HandoverType {
  UNKNOWN,
  START,                         /* Handover triggered */
  COMPLETE_SUCCESS,              /* Handover completed successfully */
  COMPLETE_FAIL,                 /* Handover failed */
  CANCEL,                        /* Handover cancelled */
  NOT_TRIGGERED,                 /* Handover not triggered */
  NOT_TRIGGERED_MOBILE_DATA_OFF, /* Handover not triggered due to mobile data off */
};
inline std::string toString(const HandoverType &o) {
  if (o == HandoverType::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == HandoverType::START) {
    return "START";
  }
  if (o == HandoverType::COMPLETE_SUCCESS) {
    return "COMPLETE_SUCCESS";
  }
  if (o == HandoverType::COMPLETE_FAIL) {
    return "COMPLETE_FAIL";
  }
  if (o == HandoverType::CANCEL) {
    return "CANCEL";
  }
  if (o == HandoverType::NOT_TRIGGERED) {
    return "NOT_TRIGGERED";
  }
  if (o == HandoverType::NOT_TRIGGERED_MOBILE_DATA_OFF) {
    return "NOT_TRIGGERED_MOBILE_DATA_OFF";
  }
  return "<invalid>";
}

enum class VowifiQuality {
  UNKNOWN,
  NONE,      /* None */
  EXCELLENT, /* Excellent */
  FAIR,      /* Fair */
  BAD        /* Bad */
};

inline std::string toString(const VowifiQuality &o) {
  if (o == VowifiQuality::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == VowifiQuality::NONE) {
    return "NONE";
  }
  if (o == VowifiQuality::EXCELLENT) {
    return "EXCELLENT";
  }
  if (o == VowifiQuality::FAIR) {
    return "FAIR";
  }
  if (o == VowifiQuality::BAD) {
    return "BAD";
  }
  return "<invalid>";
}

enum class BlockReasonType {
  UNKNOWN,
  PDP_FAILURE,          /* Pdp failure */
  REGISTRATION_FAILURE, /* Registration failure */
  HANDOVER_FAILURE,     /* Handover failure */
  OTHER_FAILURE         /* Other failures */
};
inline std::string toString(const BlockReasonType &o) {
  if (o == BlockReasonType::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == BlockReasonType::PDP_FAILURE) {
    return "PDP_FAILURE";
  }
  if (o == BlockReasonType::REGISTRATION_FAILURE) {
    return "REGISTRATION_FAILURE";
  }
  if (o == BlockReasonType::HANDOVER_FAILURE) {
    return "HANDOVER_FAILURE";
  }
  if (o == BlockReasonType::OTHER_FAILURE) {
    return "OTHER_FAILURE";
  }
  return "<invalid>";
}

enum class RegFailureReasonType {
  UNKNOWN,
  UNSPECIFIED,          /* Unspecified */
  MOBILE_IP,            /*Mobile IP */
  INTERNAL,             /* Internal */
  CALL_MANAGER_DEFINED, /* Call manager defined  */
  SPEC_DEFINED,         /* specification defined */
  PPP,                  /* PPP */
  EHRPD,                /* EHRPD */
  IPV6,                 /* IPv6 */
  IWLAN,                /* IWLAN */
  HANDOFF               /* Handoff */
};
inline std::string toString(const RegFailureReasonType &o) {
  if (o == RegFailureReasonType::UNKNOWN) {
    return "UNKNOWN";
  }
  if (o == RegFailureReasonType::UNSPECIFIED) {
    return "UNSPECIFIED";
  }
  if (o == RegFailureReasonType::MOBILE_IP) {
    return "MOBILE_IP";
  }
  if (o == RegFailureReasonType::INTERNAL) {
    return "INTERNAL";
  }
  if (o == RegFailureReasonType::CALL_MANAGER_DEFINED) {
    return "CALL_MANAGER_DEFINED";
  }
  if (o == RegFailureReasonType::SPEC_DEFINED) {
    return "SPEC_DEFINED";
  }
  if (o == RegFailureReasonType::PPP) {
    return "PPP";
  }
  if (o == RegFailureReasonType::EHRPD) {
    return "EHRPD";
  }
  if (o == RegFailureReasonType::IPV6) {
    return "IPV6";
  }
  if (o == RegFailureReasonType::IWLAN) {
    return "IWLAN";
  }
  if (o == RegFailureReasonType::HANDOFF) {
    return "HANDOFF";
  }
  return "<invalid>";
}

enum class ConfigItem {
    NONE,
    VOCODER_AMRMODESET,
    VOCODER_AMRWBMODESET,
    SIP_SESSION_TIMER,
    MIN_SESSION_EXPIRY,
    CANCELLATION_TIMER,
    T_DELAY,
    SILENT_REDIAL_ENABLE,
    SIP_T1_TIMER,
    SIP_T2_TIMER,
    SIP_TF_TIMER,
    VLT_SETTING_ENABLED,
    LVC_SETTING_ENABLED,
    DOMAIN_NAME,
    SMS_FORMAT,
    SMS_OVER_IP,
    PUBLISH_TIMER,
    PUBLISH_TIMER_EXTENDED,
    CAPABILITIES_CACHE_EXPIRATION,
    AVAILABILITY_CACHE_EXPIRATION,
    CAPABILITIES_POLL_INTERVAL,
    SOURCE_THROTTLE_PUBLISH,
    MAX_NUM_ENTRIES_IN_RCL,
    CAPAB_POLL_LIST_SUB_EXP,
    GZIP_FLAG,
    EAB_SETTING_ENABLED,
    MOBILE_DATA_ENABLED,
    VOICE_OVER_WIFI_ENABLED,
    VOICE_OVER_WIFI_ROAMING,
    VOICE_OVER_WIFI_MODE,
    CAPABILITY_DISCOVERY_ENABLED,
    EMERGENCY_CALL_TIMER,
    SSAC_HYSTERESIS_TIMER,
    VOLTE_USER_OPT_IN_STATUS,
    LBO_PCSCF_ADDRESS,
    KEEP_ALIVE_ENABLED,
    REGISTRATION_RETRY_BASE_TIME_SEC,
    REGISTRATION_RETRY_MAX_TIME_SEC,
    SPEECH_START_PORT,
    SPEECH_END_PORT,
    SIP_INVITE_REQ_RETX_INTERVAL_MSEC,
    SIP_INVITE_RSP_WAIT_TIME_MSEC,
    SIP_INVITE_RSP_RETX_WAIT_TIME_MSEC,
    SIP_NON_INVITE_REQ_RETX_INTERVAL_MSEC,
    SIP_NON_INVITE_TXN_TIMEOUT_TIMER_MSEC,
    SIP_INVITE_RSP_RETX_INTERVAL_MSEC,
    SIP_ACK_RECEIPT_WAIT_TIME_MSEC,
    SIP_ACK_RETX_WAIT_TIME_MSEC,
    SIP_NON_INVITE_REQ_RETX_WAIT_TIME_MSEC,
    SIP_NON_INVITE_RSP_RETX_WAIT_TIME_MSEC,
    AMR_WB_OCTET_ALIGNED_PT,
    AMR_WB_BANDWIDTH_EFFICIENT_PT,
    AMR_OCTET_ALIGNED_PT,
    AMR_BANDWIDTH_EFFICIENT_PT,
    DTMF_WB_PT,
    DTMF_NB_PT,
    AMR_DEFAULT_MODE,
    SMS_PSI,
    VIDEO_QUALITY,
    THRESHOLD_LTE1,
    THRESHOLD_LTE2,
    THRESHOLD_LTE3,
    THRESHOLD_1x,
    THRESHOLD_WIFI_A,
    THRESHOLD_WIFI_B,
    T_EPDG_LTE,
    T_EPDG_WIFI,
    T_EPDG_1x,
    VWF_SETTING_ENABLED,
    VCE_SETTING_ENABLED,
    RTT_SETTING_ENABLED,
    SMS_APP,
    VVM_APP,
    VOICE_OVER_WIFI_ROAMING_MODE,
    AUTO_REJECT_CALL_ENABLED,
    INVALID_CONFIG
};

enum class ConfigFailureCause {
    NO_ERR,
    IMS_NOT_READY,
    FILE_NOT_AVAILABLE,
    READ_FAILED,
    WRITE_FAILED,
    OTHER_INTERNAL_ERR,
    FAILURE_INVALID
};
inline std::string toString(const ConfigFailureCause &o) {
  if (o == ConfigFailureCause::NO_ERR) {
    return "NO_ERR";
  }
  if (o == ConfigFailureCause::IMS_NOT_READY) {
    return "IMS_NOT_READY";
  }
  if (o == ConfigFailureCause::FILE_NOT_AVAILABLE) {
    return "FILE_NOT_AVAILABLE";
  }
  if (o == ConfigFailureCause::READ_FAILED) {
    return "READ_FAILED";
  }
  if (o == ConfigFailureCause::WRITE_FAILED) {
    return "WRITE_FAILED";
  }
  if (o == ConfigFailureCause::OTHER_INTERNAL_ERR) {
    return "OTHER_INTERNAL_ERR";
  }
  if (o == ConfigFailureCause::FAILURE_INVALID) {
    return "FAILURE_INVALID";
  }
  return "<invalid>";
}

class Registration : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(RegState, State);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, ErrorCode);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, ErrorMessage);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(RIL_RadioTechnology, RadioTechnology);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, PAssociatedUris);

 public:
  Registration() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(State);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(ErrorCode);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(ErrorMessage);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(RadioTechnology);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(PAssociatedUris);
  }
  Registration(const Registration &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, State);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, ErrorCode);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, ErrorMessage);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, RadioTechnology);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, PAssociatedUris);
  }
  virtual ~Registration() {}
};

class AccessTechnologyStatus : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(RIL_RadioTechnology, NetworkMode);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(StatusType, StatusType);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, RestrictCause);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_SHARED_PTR(Registration, Registration);

 public:
  AccessTechnologyStatus() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(NetworkMode);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(StatusType);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(RestrictCause);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Registration);
  }
  AccessTechnologyStatus(const AccessTechnologyStatus &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, NetworkMode);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, StatusType);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, RestrictCause);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, Registration);
  }
  virtual ~AccessTechnologyStatus() {}
};

class ServiceStatusInfo : public qcril::interfaces::BasePayload {
  // QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(bool, IsValid);
  // QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(ServiceType, ServiceType);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(CallType, CallType);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(StatusType, StatusType);
  // QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_VECTOR(uint8_t, UserData);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint8_t, RestrictCause);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_SHARED_PTR(AccessTechnologyStatus, AccessTechnologyStatus);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(RttMode, RttMode);

 public:
  ServiceStatusInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(CallType);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(StatusType);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(RestrictCause);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(AccessTechnologyStatus);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(RttMode);
  }
  ServiceStatusInfo(const ServiceStatusInfo &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, CallType);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, StatusType);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, RestrictCause);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, AccessTechnologyStatus);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, RttMode);
  }
  virtual ~ServiceStatusInfo() {}
};

class ServiceStatusInfoList : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_VECTOR(ServiceStatusInfo, ServiceStatusInfo);

 public:
  ServiceStatusInfoList() { QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR_VECTOR(ServiceStatusInfo); }
  ServiceStatusInfoList(const ServiceStatusInfoList &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR_VECTOR(from, ServiceStatusInfo);
  }
  virtual ~ServiceStatusInfoList() {}
};

class SipErrorInfo : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, ErrorCode);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, ErrorString);

 public:
  SipErrorInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(ErrorCode);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(ErrorString);
  }

  SipErrorInfo(const SipErrorInfo &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, ErrorCode);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, ErrorString);
  }
};

class RtpStatisticsData : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint64_t, Count);

public:
  RtpStatisticsData() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Count);
  }
  RtpStatisticsData(uint64_t count) {
    setCount(count);
  }
  ~RtpStatisticsData() {};
};

class ImsSubConfigInfo : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, SimultStackCount);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, Count);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_VECTOR(uint8_t, ImsStackEnabled);

public:
  ImsSubConfigInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(SimultStackCount);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Count);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR_VECTOR(ImsStackEnabled);
  }
  ~ImsSubConfigInfo() {};
};

class AddressInfo {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, City);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, State);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, Country);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, PostalCode);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, CountryCode);
public:
  AddressInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(City);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(State);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Country);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(PostalCode);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(CountryCode);
  }
  AddressInfo(const AddressInfo &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, City);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, State);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, Country);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, PostalCode);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, CountryCode);
  }
  ~AddressInfo() { };
};


class GeoLocationInfo : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(double, Latitude);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(double, Longitude);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(AddressInfo, AddressInfo);
public:
  GeoLocationInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Latitude);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Longitude);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(AddressInfo);
  }
  GeoLocationInfo(const GeoLocationInfo &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, Latitude);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, Longitude);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, AddressInfo);
  }
  ~GeoLocationInfo() {};
};

class SsacInfo : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringFactorVoice);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringTimeVoice);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringFactorVideo);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringTimeVideo);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringFactorVoiceSib);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringTimeVoiceSib);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringFactorVideoSib);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, BarringTimeVideoSib);
public:
  SsacInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringFactorVoice);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringTimeVoice);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringFactorVideo);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringTimeVideo);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringFactorVoiceSib);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringTimeVoiceSib);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringFactorVideoSib);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BarringTimeVideoSib);
  }
  SsacInfo(const SsacInfo &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringFactorVoice);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringTimeVoice);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringFactorVideo);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringTimeVideo);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringFactorVoiceSib);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringTimeVoiceSib);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringFactorVideoSib);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BarringTimeVideoSib);
  }
  ~SsacInfo() {}
};

class BlockReasonDetails {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(RegFailureReasonType, RegFailureReasonType);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, RegFailureReason);
public:
  BlockReasonDetails() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(RegFailureReasonType);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(RegFailureReason);
  }
  BlockReasonDetails(const BlockReasonDetails &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, RegFailureReasonType);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, RegFailureReason);
  }
  ~BlockReasonDetails() {}

};

class BlockStatus {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(BlockReasonType, BlockReason);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_SHARED_PTR(BlockReasonDetails, BlockReasonDetails);
public:
  BlockStatus() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BlockReason);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BlockReasonDetails);
  }
  BlockStatus(const BlockStatus &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BlockReason);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BlockReasonDetails);
  }
  ~BlockStatus() {}
};

class RegistrationBlockStatus : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_SHARED_PTR(BlockStatus, BlockStatusOnWwan);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR_SHARED_PTR(BlockStatus, BlockStatusOnWlan);
public:
  RegistrationBlockStatus() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BlockStatusOnWwan);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BlockStatusOnWlan);
  }
  RegistrationBlockStatus(const RegistrationBlockStatus &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BlockStatusOnWwan);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BlockStatusOnWlan);
  }
  ~RegistrationBlockStatus() {}
};

class ConfigInfo : public qcril::interfaces::BasePayload {
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(ConfigItem,Item);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(bool, BoolValue);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(uint32_t, IntValue);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(std::string, StringValue);
  QCRIL_INTERFACE_OPTIONAL_MEMBER_VAR(ConfigFailureCause, ErrorCause);
public:
  ConfigInfo() {
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(Item);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(BoolValue);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(IntValue);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(StringValue);
    QCRIL_INTERFACE_RESET_OPTIONAL_MEMBER_VAR(ErrorCause);
  }
  ConfigInfo(const ConfigInfo &from) {
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, Item);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, BoolValue);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, IntValue);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, StringValue);
    QCRIL_INTERFACE_COPY_OPTIONAL_MEMBER_VAR(from, ErrorCause);
  }

  ~ConfigInfo() {}
};

struct MultiIdentityInfo{
    static const int LINE_TYPE_UNKNOWN = 0;
    static const int LINE_TYPE_PRIMARY = 1;
    static const int LINE_TYPE_SECONDARY = 2;

    static const int REGISTRATION_STATUS_UNKNOWN = 0;
    static const int REGISTRATION_STATUS_ACTIVE = 1;
    static const int REGISTRATION_STATUS_INACTIVE = 2;

    std::string msisdn;
    int registrationStatus;
    int lineType;

    MultiIdentityInfo(std::string msisdn = std::string(),
            int registrationStatus = REGISTRATION_STATUS_UNKNOWN,
            int lineType = LINE_TYPE_UNKNOWN) {
        this->msisdn = msisdn;
        this->registrationStatus = registrationStatus;
        this->lineType = lineType;
    }
    MultiIdentityInfo(const MultiIdentityInfo &from) {
        msisdn = from.msisdn;
        registrationStatus = from.registrationStatus;
        lineType = from.lineType;
    }
};

class VirtualLineInfo : public qcril::interfaces::BasePayload {
private:
  std::string mMsisdn;
  std::vector<std::string> mVirtualLines;
public:
  VirtualLineInfo() {};
  VirtualLineInfo(const std::string& msisdn, const std::vector<std::string> lines) :
    mMsisdn(msisdn), mVirtualLines(lines) {
  };
  VirtualLineInfo(const VirtualLineInfo &from) {
    mMsisdn = from.mMsisdn;
    mVirtualLines = from.mVirtualLines;
  }
  ~VirtualLineInfo() {};

  std::string getMsisdn() { return mMsisdn; }
  std::vector<std::string>& getVirtualLines() { return mVirtualLines; };
};

} // namespace interfaces
} // namespace qcril
