#pragma once

/*===========================================================================

  Copyright (c) 2017, 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

===========================================================================*/

#include "framework/Message.h"
#include <framework/legacy.h>
#include "DataCommon.h"
#include "framework/message_translator.h"
#include "framework/SolicitedMessage.h"
#include "framework/GenericCallback.h"
#include "framework/add_message_id.h"
#include "legacy/qmi_embms_v01.h"
#include <modules/android/RilRequestMessage.h>

namespace rildata {

class RilRequestEmbmsContentDescUpdateMessage : public SolicitedMessage<generic_callback_payload>,
                                                public add_message_id<RilRequestEmbmsContentDescUpdateMessage> {
    private:

    legacy_request_payload params;
    embms_update_content_desc_req_msg_v01 content_desc_update;

    public:
    static constexpr const char* MESSAGE_NAME = "HOOK_EMBMS_UNSOL_CONTENT_DESC_CONTROL";
    RilRequestEmbmsContentDescUpdateMessage() = delete;

    RilRequestEmbmsContentDescUpdateMessage(const qcril_request_params_type *request)
    :SolicitedMessage<generic_callback_payload>(get_class_message_id()){
        mName = MESSAGE_NAME;
        params.get_params().instance_id = request->instance_id;
        params.get_params().modem_id = request->modem_id;
        params.get_params().event_id_android = request->event_id_android;
        params.get_params().event_id = request->event_id;
        params.get_params().datalen = request->datalen;
        params.get_params().t = request->t;
        params.get_params().response_callback = request->response_callback;
        memcpy(&content_desc_update, request->data, sizeof(embms_update_content_desc_req_msg_v01));
        params.get_params().data = static_cast<void*>(&content_desc_update);

    }
    ~RilRequestEmbmsContentDescUpdateMessage();

    qcril_request_params_type &get_params() {
          return params.get_params();
    }
    string dump();
};

}//namespace
