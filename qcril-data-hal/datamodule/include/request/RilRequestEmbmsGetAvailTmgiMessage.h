#pragma once

/*===========================================================================

  Copyright (c) 2017, 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

===========================================================================*/

#include "framework/Message.h"
#include <framework/legacy.h>
#include "framework/SolicitedMessage.h"
#include "framework/GenericCallback.h"
#include "framework/add_message_id.h"
#include "framework/message_translator.h"
#include "DataCommon.h"
#include "legacy/qmi_embms_v01.h"
#include <modules/android/RilRequestMessage.h>

namespace rildata {

class RilRequestEmbmsGetAvailTmgiMessage : public SolicitedMessage<generic_callback_payload>,
                                           public add_message_id<RilRequestEmbmsGetAvailTmgiMessage>  {
    private:

    legacy_request_payload params;
    embms_get_available_tmgi_req_msg_v01 avail_tmgi_req;

    public:
    static constexpr const char* MESSAGE_NAME = "HOOK_EMBMS_UNSOL_AVAILABLE_TMGI";
    RilRequestEmbmsGetAvailTmgiMessage() = delete;

    RilRequestEmbmsGetAvailTmgiMessage(const qcril_request_params_type *request)
    :SolicitedMessage<generic_callback_payload>(get_class_message_id()){
        mName = MESSAGE_NAME;
        params.get_params().instance_id = request->instance_id;
        params.get_params().modem_id = request->modem_id;
        params.get_params().event_id_android = request->event_id_android;
        params.get_params().event_id = request->event_id;
        params.get_params().datalen = request->datalen;
        params.get_params().t = request->t;
        params.get_params().response_callback = request->response_callback;
        memcpy(&avail_tmgi_req, request->data, sizeof(embms_get_available_tmgi_req_msg_v01));
        params.get_params().data = static_cast<void*>(&avail_tmgi_req);
    }
    ~RilRequestEmbmsGetAvailTmgiMessage();

    qcril_request_params_type &get_params() {
          return params.get_params();
    }
    string dump();
};

}//namespace
