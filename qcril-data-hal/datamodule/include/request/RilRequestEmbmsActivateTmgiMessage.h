#pragma once

/*===========================================================================

  Copyright (c) 2017, 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

===========================================================================*/

#include "framework/Message.h"
#include <framework/legacy.h>
#include "DataCommon.h"
#include "framework/message_translator.h"
#include "framework/SolicitedMessage.h"
#include "framework/GenericCallback.h"
#include "framework/add_message_id.h"
#include "legacy/qmi_embms_v01.h"
#include <modules/android/RilRequestMessage.h>

namespace rildata {

class RilRequestEmbmsActivateTmgiMessage : public SolicitedMessage<generic_callback_payload>,
                                           public add_message_id<RilRequestEmbmsActivateTmgiMessage> {
    private:

    legacy_request_payload params;
    embms_activate_tmgi_req_msg_v01 activate_req;

     public:

     static constexpr const char* MESSAGE_NAME = "HOOK_EMBMS_ACTIVATE_TMGI";
     RilRequestEmbmsActivateTmgiMessage() = delete;

     RilRequestEmbmsActivateTmgiMessage(const qcril_request_params_type *request)
     :SolicitedMessage<generic_callback_payload>(get_class_message_id()){
        mName = MESSAGE_NAME;
        params.get_params().instance_id = request->instance_id;
        params.get_params().modem_id = request->modem_id;
        params.get_params().event_id_android = request->event_id_android;
        params.get_params().event_id = request->event_id;
        params.get_params().datalen = request->datalen;
        params.get_params().t = request->t;
        params.get_params().response_callback = request->response_callback;
        memcpy(&activate_req, request->data, sizeof(embms_activate_tmgi_req_msg_v01));
        params.get_params().data = static_cast<void*>(&activate_req);
    }
    ~RilRequestEmbmsActivateTmgiMessage();

    qcril_request_params_type &get_params() {
          return params.get_params();
    }
    string dump();
};

}//namespace
