/*!
 *
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 */

#pragma once

#include <vendor/qti/spu/1.0/ISpcomSSREvent.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/StrongPointer.h>

namespace vendor {
namespace qti {
namespace spu {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct SpcomSSREvent : public ISpcomSSREvent {

public:
    // Methods from ::vendor::qti::spu::V1_0::ISPUEvent follow.
    Return<void> callback() override;

    bool isSSR() { return mIsSSR; }

    // Methods from ::android::hidl::base::V1_0::IBase follow.
private:
    bool mIsSSR = false;
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace spu
}  // namespace qti
}  // namespace vendor
