/*!
 *
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 */

#include "SpcomSSREvent.h"

#include "log.h"

namespace vendor {
namespace qti {
namespace spu {
namespace V1_0 {
namespace implementation {

// Methods from ::vendor::qti::spu::V1_0::ISPUEvent follow.
Return<void> SpcomSSREvent::callback() {
    mIsSSR = true;
    return Void();
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace spu
}  // namespace qti
}  // namespace vendor
