/*!
 *
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 */

#include "SpcomSSREvent.h"
#include "SPUHidlClientTest.h"
#include "log.h"

#define MAJOR 0
#define MINOR 1

#define TEST_APP_PATH "/vendor/firmware_mnt/image/spu_hal_test.signed"
#define TEST_APP_CHANNEL "iuicc"

int main(int /*argc*/, char** /*argv*/) {

    ALOGI("vendor.spu.qti@1.0-test-client version [%d.%d] Date [%s] Time [%s]",
            MAJOR, MINOR, __DATE__, __TIME__);

    SPUHidlClientTest* testFw = new SPUHidlClientTest(TEST_APP_PATH, TEST_APP_CHANNEL);
    if(testFw == nullptr) {
        ALOGE("Failed to create framework object");
        return SPUHidlClientTest::GENERAL_ERROR;
    }

    if(testFw->readSpuInfo() != SPUHidlClientTest::SUCCESS) {
        ALOGE("Failed to read SPU info, move to the next test...");
    }

    int32_t loadResult = testFw->loadApp();
    ALOGI("loadApp() returned %d", loadResult);
    if(loadResult != SPUHidlClientTest::SUCCESS && loadResult != SPUHidlClientTest::ALREADY_LOADED) {
        ALOGE("Failed to load app [%s] with channel [%s], abort...", TEST_APP_PATH, TEST_APP_CHANNEL);
        return SPUHidlClientTest::GENERAL_ERROR;
    }

    if(testFw->sendPing() != SPUHidlClientTest::SUCCESS) {
        ALOGE("PING-PONG test failed, move to the next test...");
    }

    if(testFw->sendSharedBuffFlow() != SPUHidlClientTest::SUCCESS) {
        ALOGE("Shared buffer flow test failed, finishing...");
    }

    delete testFw;

    return 0;
}
